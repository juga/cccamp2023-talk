# CCCamp 2023 Tor Talk

Session type: 45 min talk

Recorded: yes.

## Topics
- Network Performance:
    - DoS
        - PoW +1
    - CC (talk to Mike)
        - Future tuning
        - Variance have gone down for performance
    - Conflux +1
    - Shadow simulation (tuning, etc.)
    - Bridges load balance (scanner)
    - Arti Relay paradoxes (likely a smaller network / higher performance)
    - UDP => Access to HTTP/3 + Quic

- Network Health:
    - Community intake of proposals +1
    - Pitch for relay ops meeting (do the relay ops meetup *after* our talk)
    - Overview of s112 work
        - side channels
        - anti-tagging crypto (enumerated)

- Anti-censorship:
    - New PTs: Snowflake, Webtunnel, Conjure
    - Bridge distributor (Lox)

## A guided tour into Tor network health and performance

Since the last time, we were all at camp, several significant changes have
happened within the Tor network ecosystem, both technically and socially. In
this presentation, we will review some exciting recent updates to the Tor
network and look into the world of bad relay tracking, general network health
observations, and the situation where multiple extensive Denial of Service
attacks have caused a slowdown of the overall network performance.

We wish to guide the audience through a number of new technologies that have
been added to the network. These innovations include a modern congestion control
mechanism, our multi-path circuit feature, Conflux, and a Proof-of-Work (PoW)
mechanism to help against Onion Services attacks. Additionally, we will discuss
some upcoming changes to the current C Tor code base and our journey towards a
Rust Tor relay implementation as part of our Arti re-implementation of Tor.
Finally, in addition to the technology modifications, we also would like to talk
about some of the social developments happening with the network, amongst
others, a new mechanism for handling incoming technical and social proposals
from the greater Tor community.
