---
title: State of the Onion
date: November 28, 2021
institute: Cryptohagen
author:
  - name: Alexander Færøy
    email: ahf@torproject.org
slides:
    aspect-ratio: 169
    font-size: 14pt
    table-of-contents: false
---

## About Me

\begin{columns}
    \begin{column}{0.65\textwidth}
        \begin{itemize}
            \item Core Developer at The Tor Project since early 2017.
                  Team Lead of the Network Team since late 2019.
            \item Free Software developer since 2006.
            \item Worked with distributed systems in the Erlang programming
                  language, WebKit-based mobile browsers, embedded
                  development, and software development consulting.
            \item Co-organizing the annual Danish hacker festival
                  \href{https://bornhack.dk/}{BornHack} on Funen.
        \end{itemize}
    \end{column}

    \begin{column}{0.35\textwidth}
        \begin{center}
            \includegraphics[width=0.95\textwidth]{images/tor_man.png}
        \end{center}
    \end{column}
\end{columns}

## What is Tor?

\begin{columns}
    \begin{column}{0.6\textwidth}
        \begin{itemize}
            \item Online anonymity, and censorship circumvention.
                \begin{itemize}
                    \item Free software.
                    \item Open network.
                \end{itemize}
            \item Community of researchers, developers, users, and relay operators.
            \item U.S. 501(c)(3) non-profit organization.
        \end{itemize}
    \end{column}

    \begin{column}{0.4\textwidth}
        \begin{center}
            \includegraphics[width=0.95\textwidth]{images/what_is_tor.jpg}
        \end{center}
    \end{column}
\end{columns}

## History

**Early 2000s**
  : Working with the U.S. Naval Research Laboratory.

**2004**
  : Sponsorship by the Electronic Frontier Foundation.

**2006**
  : The Tor Project, Inc. became a non-profit.

**2007**
  : Expansion to anti-censorship.

**2008**
  : Tor Browser development.

**2010**
  : The Arab spring.

**2013**
  : The summer of Snowden.

**2018**
  : Anti-censorship team created.

**2020**
  : Network Health team created.

## Miscellaneous

- We killed v2 Onions. Finally.
- New community forum.
- Matrix bridge for our IRC communications.
- v3 Onion stats on \href{https://metrics.torproject.org/}{metrics.torproject.org}.
- `MetricsPort` in Tor.
- Middle-only relays specified.
- Vanguards Lite in Tor.
- Snowflake PT progress.

## {.plain}

\tikzset{external/export next=false}
\begin{tikzpicture}[remember picture, overlay, background rectangle/.style={fill=OnionDarkPurple}, show background rectangle]
    \node[text=white, at=(current page.center), font=\bfseries] {Speed > Blocking > Privacy > Security > UI};
\end{tikzpicture}

## The Tor Network

\centering
\begin{tikzpicture}
    \begin{axis}[
            title=Total Relay Bandwidth,
            title style={font=\scriptsize\bfseries},
            no markers,
            enlarge x limits=false,
            grid=both,
            grid style=dashed,
            width=0.85\paperwidth,
            height=0.80\paperheight,
            date coordinates in=x,
            xmin=2010-01-01,
            xmax=2022-01-01,
            xtick={
                {2010-01-01},
                {2011-01-01},
                {2012-01-01},
                {2013-01-01},
                {2014-01-01},
                {2015-01-01},
                {2016-01-01},
                {2017-01-01},
                {2018-01-01},
                {2019-01-01},
                {2020-01-01},
                {2021-01-01},
                {2022-01-01}
            },
            cycle list name=exotic,
            every axis plot/.append style={thick},
            label style={font=\scriptsize},
            tick label style={font=\scriptsize},
            legend style={
                font=\tiny,
            },
            legend pos=north west,
            legend cell align=left,
            unbounded coords=discard,
            xticklabel style={
                anchor=near xticklabel,
            },
            ylabel={Bandwidth in Gbit/s},
            xticklabel=\year\
            ]

        \addlegendentry{Advertised Bandwidth}
        \addplot table [x=date, y=advbw, col sep=comma] {data/bandwidth-flags.csv};

        \addlegendentry{Bandwidth History}
        \addplot table [x=date, y=bwhist, col sep=comma] {data/bandwidth-flags.csv};
    \end{axis}
\end{tikzpicture}

\tiny Source: \href{https://metrics.torproject.org/}{metrics.torproject.org}

## The Tor Network {.c}

\centering
\includegraphics[width=0.6\textwidth]{images/network-status.png}

\large \href{https://status.torproject.org/}{status.torproject.org}

## The Tor Network

\centering
\begin{tikzpicture}
    \begin{axis}[
            title=Total Relay Bandwidth in 2021,
            title style={font=\scriptsize\bfseries},
            no markers,
            enlarge x limits=false,
            grid=both,
            grid style=dashed,
            width=0.85\paperwidth,
            height=0.80\paperheight,
            date coordinates in=x,
            xmin=2021-01-01,
            xmax=2021-12-01,
            xtick={
                {2021-01-01},
                {2021-02-01},
                {2021-03-01},
                {2021-04-01},
                {2021-05-01},
                {2021-06-01},
                {2021-07-01},
                {2021-08-01},
                {2021-09-01},
                {2021-10-01},
                {2021-11-01},
                {2021-12-01}
            },
            cycle list name=exotic,
            every axis plot/.append style={thick},
            label style={font=\scriptsize},
            tick label style={font=\scriptsize},
            legend style={
                font=\tiny,
            },
            legend pos=north west,
            legend cell align=left,
            unbounded coords=discard,
            xticklabel style={
                anchor=near xticklabel,
            },
            ylabel={Bandwidth in Gbit/s},
            xticklabel=\month\
            ]

        \addlegendentry{Advertised Bandwidth}
        \addplot table [x=date, y=advbw, col sep=comma] {data/bandwidth-flags.csv};

        \addlegendentry{Bandwidth History}
        \addplot table [x=date, y=bwhist, col sep=comma] {data/bandwidth-flags.csv};
    \end{axis}
\end{tikzpicture}

\tiny Source: \href{https://metrics.torproject.org/}{metrics.torproject.org}

## The Tor Network

\centering
\begin{tikzpicture}
    \begin{axis}[
            title=Number of Relays,
            title style={font=\scriptsize\bfseries},
            no markers,
            enlarge x limits=false,
            grid=both,
            grid style=dashed,
            width=0.85\paperwidth,
            height=0.80\paperheight,
            date coordinates in=x,
            xmin=2010-01-01,
            xmax=2022-01-01,
            xtick={
                {2010-01-01},
                {2011-01-01},
                {2012-01-01},
                {2013-01-01},
                {2014-01-01},
                {2015-01-01},
                {2016-01-01},
                {2017-01-01},
                {2018-01-01},
                {2019-01-01},
                {2020-01-01},
                {2021-01-01},
                {2022-01-01}
            },
            cycle list name=exotic,
            every axis plot/.append style={thick},
            label style={font=\scriptsize},
            tick label style={font=\scriptsize},
            legend style={
                font=\tiny,
            },
            legend pos=north west,
            legend cell align=left,
            unbounded coords=discard,
            xticklabel style={
                anchor=near xticklabel,
            },
            xticklabel=\year\
            ]

        \addlegendentry{Relays}
        \addplot table [x=date, y=relays, col sep=comma] {data/networksize.csv};

        \addlegendentry{Bridges}
        \addplot table [x=date, y=bridges, col sep=comma] {data/networksize.csv};
    \end{axis}
\end{tikzpicture}

\tiny Source: \href{https://metrics.torproject.org/}{metrics.torproject.org}

## The Tor Network

\centering
\begin{tikzpicture}
    \begin{axis}[
            title=Number of Relays in 2021,
            title style={font=\scriptsize\bfseries},
            no markers,
            enlarge x limits=false,
            grid=both,
            grid style=dashed,
            width=0.85\paperwidth,
            height=0.80\paperheight,
            date coordinates in=x,
            xmin=2021-01-01,
            xmax=2021-12-01,
            ymin=0,
            ymax=9000,
            xtick={
                {2021-01-01},
                {2021-02-01},
                {2021-03-01},
                {2021-04-01},
                {2021-05-01},
                {2021-06-01},
                {2021-07-01},
                {2021-08-01},
                {2021-09-01},
                {2021-10-01},
                {2021-11-01},
                {2021-12-01}
            },
            cycle list name=exotic,
            every axis plot/.append style={thick},
            label style={font=\scriptsize},
            tick label style={font=\scriptsize},
            legend style={
                font=\tiny,
            },
            legend pos=north west,
            legend cell align=left,
            unbounded coords=discard,
            xticklabel style={
                anchor=near xticklabel,
            },
            xticklabel=\month
            ]

        \addlegendentry{Relays}
        \addplot table [x=date, y=relays, col sep=comma] {data/networksize.csv};

        \addlegendentry{Bridges}
        \addplot table [x=date, y=bridges, col sep=comma] {data/networksize.csv};
    \end{axis}
\end{tikzpicture}

## {.plain}

\tikzset{external/export next=false}
\begin{tikzpicture}[remember picture, overlay, background rectangle/.style={fill=OnionDarkPurple}, show background rectangle]
    \node[text=white, at=(current page.center), font=\bfseries] {Bad Relays and Bandwidth Scanning};
\end{tikzpicture}

## Arti

Arti is our new Tor client implementation in the Rust programming language.

Focus on building libraries to work with the entire Tor ecosystem.

## Arti

Why rewrite though?

21 out of 34 of Tor's TROVE's was related to memory issues allowed by the C
programming language.

## Arti

**0.1.0**
  : API stability.

**1.0.0**
  : Usability, performance, and stability.

**1.1.0**
  : Anti-censorship.

**1.2.0**
  : Onion services.

**2.0.0**
  : Ready to replace the C client.

**???**
  : Relay support?

## VPN-mode

- Build on top of Arti.
- Collaboration with Guardian Project and LEAP Encryption Access Project.
- A ton of anonymity and privacy challenges.
- TCP and UDP support.
- Android is the first target platform and we aim for releasing in 2023.

## UDP Support in the Tor Network

Support UDP for Tor clients and Exit nodes to allow support for modern internet
applications such as: Crypto wallets, streaming, VoIP, and \emph{hopefully}
WebRTC-based applications.

Client and Exit nodes will require upgrades for deployment.

Use Tor's Congestion Control system to decide when to drop packets at the edges.

Specification work being tracked in
\href{https://gitlab.torproject.org/tpo/core/torspec/-/issues/73}{torspec\#73}
on \href{https://gitlab.torproject.org/}{Tor's Gitlab}.

## UDP Support in the Tor Network

\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]
    \tikzstyle{wiretap}=[circle, draw, thin, fill=red!40, scale=0.8]

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    \node[] at (0, 2) {The Tor Network};
    \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1] at (0,0) {};

    %% The relay nodes inside the Anonymity Network cloud.
    \node[relay] (r1) at (-1.9, 0.2)  {$R_{1}$};
    \node[relay] (r2) at (0.0, 0.1)   {$R_{2}$};
    \node[relay] (r3) at (1.8, -0.4)  {$R_{3}$};
    \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
    \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
    \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
    \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};

    \path[line width=4.0pt] (r1) edge (r2);
    \path[ultra thick, dashed, color=green] (r1) edge (r2);

    \path[line width=4.0pt] (r2) edge (r7);
    \path[ultra thick, dashed, color=green] (r2) edge (r7);

    %% Path between Alice and R1.
    \path[line width=4.0pt] (-4.4, -0.4) edge (r1);
    \path[ultra thick, dashed, color=green] (-4.4, -0.4) edge (r1);

    \path[ultra thick, dashed, color=green] (r7) edge (4.4, -0.4);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}

## New Congestion Control

- Goal is to target 0.4.7 (release early 2022).
- End-point upgrades only.
- Should get rid of the ~500 kb/sec "barrier".
- Parameter tuning with Shadow is ongoing right now with the Network and Metrics team.
- Many people has been involved in this and much research was made.

## Tokens

- Reducing exit traffic blocking?
- DoS prevention?
- Later: Bridge distribution?

Chaum Tokens and PrivacyPass seems to be where the interest is right now.

## Conflux

The goal is to overcome Tor's network performance bottlenecks using \textbf{Traffic Splitting}.

Current work specified by David Goulet and Mike Perry in Tor's
\href{https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/329-traffic-splitting.txt}{Proposal
\#329}.

Based on work by Mashael AlSabah, Kevin Bauer, Tariq Elahi, and Ian Goldberg in
the paper \href{https://www.freehaven.net/anonbib/papers/pets2013/paper_65.pdf}{The Path
Less Travelled: Overcoming Tor’s Bottlenecks with Traffic Splitting}.

Prototyping and Shadow experimentation will begin in \textbf{early 2022}.

## Conflux {.t}

\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]
    \tikzstyle{wiretap}=[circle, draw, thin, fill=red!40, scale=0.8]

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    \node[] at (0, 2) {The Tor Network};
    \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1] at (0,0) {};

    %% The relay nodes inside the Anonymity Network cloud.
    \node[relay] (r1) at (-1.9, 0.2)  {$R_{1}$};
    \node[relay] (r2) at (0.0, 0.1)   {$R_{2}$};
    \node[relay] (r3) at (1.8, -0.4)  {$R_{3}$};
    \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
    \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
    \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
    \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};

    \path[thick] (r1) edge (r2);
    \path[thick] (r2) edge (r7);

    %% Path between Alice and R1.
    \path[thick] (-4.4, -0.4) edge (r1);

    \path[thick] (r7) edge (4.4, -0.4);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}

## Conflux {.t}

\centering
\begin{tikzpicture}
    %% Define the style for our relay nodes inside the Anonymity Network cloud.
    \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]
    \tikzstyle{wiretap}=[circle, draw, thin, fill=red!40, scale=0.8]

    %% Alice.
    \node[] at (-6, 2.5) {Alice};
    \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

    %% Bob.
    \node[] at (6, 2.5) {Bob};
    \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

    \node[] at (0, 2) {The Tor Network};
    \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1] at (0,0) {};

    %% The relay nodes inside the Anonymity Network cloud.
    \node[relay] (r1) at (-1.9, 0.2)  {$R_{1}$};
    \node[relay] (r2) at (0.0, 0.1)   {$R_{2}$};
    \node[relay] (r3) at (1.8, -0.4)  {$R_{3}$};
    \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
    \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
    \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
    \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};

    \path[thick] (r1) edge (r2);
    \path[thick] (r2) edge (r7);
    \path[thick] (r5) edge (r6);
    \path[thick] (r6) edge (r7);

    %% Path between Alice and R1.
    \path[thick] (-4.4, -0.4) edge (r1);

    %% Path between Alice and R5
    \path[thick] (-4.4, -0.4) edge (r5);

    \path[thick] (r7) edge (4.4, -0.4);

    %% Helper lines for debugging.
    %% \draw[help lines] (-7,-3) grid (7,3);
\end{tikzpicture}

## {.c .plain .noframenumbering}

\centering

\vfill

\Large \color{OnionPurple}{Run a Tor Bridge}

\vfill

\normalsize

\includegraphics[scale=0.4]{images/bridge.png}

## Run a Tor bridge

- We have ~900 obfs4 bridges for ~60.000 users.
- Goal is \textbf{200 new obfs4 bridges!}
- Start 10 new bridges, get a hoodie \color{OnionPurple}{:-)}

## How can you help?

\centering
Help test our Alpha releases

\vfill

\begin{columns}
    \begin{column}{0.5\textwidth}
        \centering
        \includegraphics[width=0.6\textwidth]{images/tor_browser_logo_alpha.png}
    \end{column}

    \begin{column}{0.5\textwidth}
        \centering
        \includegraphics[width=0.6\textwidth]{images/tor_browser_logo_nightly.png}
    \end{column}
\end{columns}

\vfill

\large\href{https://www.torproject.org/download/alpha/}{torproject.org/download/alpha/}

## How can you help?

\begin{columns}
    \begin{column}{0.65\textwidth}
        \begin{itemize}
            \item Run a Tor relay or \textbf{a bridge!}
            \item Teach others about Tor and privacy in general.
            \item Find, and maybe fix, bugs in Tor.
            \item Test Tor on your platform of choice.
            \item Work on some of the many open research projects.
            \item Donate at \href{https://donate.torproject.org/}{donate.torproject.org}
        \end{itemize}
    \end{column}

    \begin{column}{0.35\textwidth}
        \begin{center}
            \includegraphics[width=0.95\textwidth]{images/tor_peace.png}
        \end{center}
    \end{column}
\end{columns}

## {.c .plain .noframenumbering}

\centering

\vfill

\huge Questions?

\vfill

\normalsize

\begin{columns}
    \begin{column}{0.60\textwidth}
        \begin{itemize}
            \setlength\itemsep{0.75em}

            \item[\faTwitter] \colorhref{https://twitter.com/ahfaeroey}{@ahfaeroey}
            \item[\faEnvelope] \colorhref{mailto:ahf@torproject.org}{ahf@torproject.org}
            \item[\faKey] OpenPGP: \\ \texttt{1C1B C007 A9F6 07AA 8152} \\ \texttt{C040 BEA7 B180 B149 1921}
        \end{itemize}
    \end{column}

    \begin{column}{0.40\textwidth}
        \begin{center}
            \includegraphics[width=0.95\textwidth]{images/tor_bike.png}
        \end{center}
    \end{column}
\end{columns}

## {.c .plain .noframenumbering}

\centering

This work is licensed under a

\large \href{https://creativecommons.org/licenses/by-sa/4.0/}{Creative Commons \\ Attribution-ShareAlike 4.0 International License}

\vfill

\ccbysa

\vfill
